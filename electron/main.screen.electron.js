const { BrowserWindow, app, ipcMain, Menu } = require('electron');
const url = require('url');
const path = require('path');
const screens = require('./screens.electron.js');


const mainScreenMenu = [
    {
        label: 'DevTool',
        submenu: [
            {
                label: 'Console',
                accelerator: 'F12',
                click(item, focused_window) {
                    focused_window.toggleDevTools();
                }
            },
            {
                role: 'reload',
                accelerator: 'F5'
            }
        ]
    }
];


const createMainUI = () => {
    const mainScreen = new BrowserWindow({
        webPreferences: {
            nodeIntegration: true,
            contextIsolation: false,
            enableRemoteModule: true,
        },
    });

    mainScreen.maximize();



    const thisMenu = Menu.buildFromTemplate(mainScreenMenu);
    Menu.setApplicationMenu(thisMenu)

    mainScreen.loadURL(url.format({
        pathname: path.join(__dirname, '../html/index.html'),
        protocol: 'file',
        slashes: true
    }))

    screens['mainScreen'] = mainScreen;

}

module.exports = createMainUI;