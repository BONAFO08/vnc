const { BrowserWindow, app, ipcMain, Menu } = require('electron');
const url = require('url');
const path = require('path');
const util = require('util');
const fs = require('fs');
const createMainUI = require('./electron/main.screen.electron.js');
const {} = require('./electron/events.electron.js');

if (process.env.NODE_ENV !== 'production') {
    require('electron-reload')(__dirname, {
    })

}


app.on('ready',()=>{
    createMainUI();
})