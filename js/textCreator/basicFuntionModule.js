function myFunction1() {
    //CODE
    alert(1)
}

saveFunction(myFunction1,'myFunction1');

const myFunction2 = (a) => {
    //CODE
        alert(a)
};

saveFunction(myFunction2,'myFunction2');


//METHOD 1
//TO CALL THE FUNCTION 
// SEARCH IN THE FUNCTION DATABASE ARR "myFunctionsDatabase" => THE FUNTION BY ID 
// AND MYFUNTION.execute()

/*
EXAMPLE

const myFunction = myFunctionsDatabase.filter(funct => funct.id == 'myFunction1')[0];
myFunction.execute()
*/

//METHOD 1
//USE THE FUNCTION "searchF("yourFunctionID")" => 
// THIS WILL RETURN YOUR FUNCTION
// AND MYFUNTION.execute()


/*
EXAMPLE

const myFunction = searchF("myFunction1");
myFunction.execute()
*/

