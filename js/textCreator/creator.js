

//VALIDATE THE DIR
const dirValidator = async (dir) => {
    try {
        await fs.promises.readdir(dir)
        return { bool: true, err: '' };
    } catch (error) {
        await fs.promises.mkdir(dir);
        return { bool: false, err: error };
    }
}

//LOAD FILE 
const loadFile = async (dir) => {
    try {
        const readFile = util.promisify(fs.readFile);
        const data = await readFile(dir);
        return { data: data.toString(), bool: true, err: '' }
    } catch (error) {
        return { data: { bills: [] }, bool: false, err: error }
    }
}




//CREATE FILE
const saveFile = async (dir, content) => {
    try {
        const saveFile = util.promisify(fs.writeFile);
        const file_response = await saveFile(dir, content);
        return { bool: true, err: '' }
    } catch (error) {
        return { bool: false, err: error }
    }
}


const updateContainer = async () => {
    const dirValidatorResponse = await dirValidator(databaseURI);
    if (dirValidatorResponse.err == '' || dirValidatorResponse.err.errno == -4058) {
        const loadingFileResponse = await loadFile(`${databaseURI}/container.js`);
        if (loadingFileResponse.err == '' || loadingFileResponse.err.errno == -4058) {
            const newData = `
            ${loadingFileResponse.data}
            const perro = 123;
            console.log(perro);            
            `
            const updateResponse = await saveFile(`${databaseURI}/container.js`,newData);
        } else {
        }

    } else {
        console.error(dirValidatorResponse.err)
        return false
    }
}



//SEARCH
// const getDatabase = async (cardType) => {
//     const dirValidatorResponse = await dirValidator(databaseURI);
//     if (dirValidatorResponse.err == '' || dirValidatorResponse.err.errno == -4058) {
//         const loadingFileResponse = await loadFile(`${databaseURI}/${cardType}.json`);
//         if (loadingFileResponse.err == '' || loadingFileResponse.err.errno == -4058) {
//             return loadingFileResponse.data;
//         } else {
//             console.error(dirValidatorResponse.err)
//             return false
//         }
//     } else {
//         console.error(dirValidatorResponse.err)
//         return false
//     }
// }