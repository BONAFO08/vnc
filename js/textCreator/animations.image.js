const setImageAnimation = (animationID, [oldImage, newImage]) => {
    switch (animationID) {
        case 'basic':
            newImage.css({
                margin: `-.5rem 0rem 0rem 0rem`,
                opacity: 1,
            })

            oldImage.css({
                opacity: 0,
            })
            break;


        case 'basic-plus-left':
            newImage.css({
                margin: `-.5rem 0rem 0rem 0rem`,
                opacity: 1,
            })

            oldImage.css({
                opacity: 0,
            })
            break;

        case 'basic-plus-right':
            newImage.css({
                margin: `-.5rem 0rem 0rem 0rem`,
                opacity: 1,
            })

            oldImage.css({
                opacity: 0,
            })
            break;

        case 'basic-plus-bottom':
            newImage.css({
                margin: `-.5rem 0rem 0rem 0rem`,
                opacity: 1,
            })

            oldImage.css({
                opacity: 0,
            })
            break;

        case 'basic-plus-top':
            newImage.css({
                margin: `-.5rem 0rem 0rem 0rem`,
                opacity: 1,
            })

            oldImage.css({
                opacity: 0,
            })
            break;


        case 'appear-from-left':
            newImage.css({
                margin: `-.5rem 0rem 0rem 0rem`,
                opacity: 1,
            })

            oldImage.css({
                margin: `0rem 0rem 0rem ${window.innerWidth}px`,
                opacity: 0,
            })
            break;

        case 'appear-from-right':
            newImage.css({
                margin: `-.5rem 0rem 0rem 0rem`,
                opacity: 1,
            })

            oldImage.css({
                margin: `0rem 0rem 0rem ${-window.innerWidth}px`,
                opacity: 0,
            })
            break;

        case 'appear-from-bottom':
            newImage.css({
                margin: `-.5rem 0rem 0rem 0rem`,
                opacity: 1,
            })

            oldImage.css({
                margin: `${window.innerHeight}px 0rem 0rem 0rem`,
                opacity: 0,
            })
            break;

        case 'appear-from-top':

            newImage.css({
                margin: `-.5rem 0rem 0rem 0rem`,
                opacity: 1,
            })

            oldImage.css({
                margin: `${-window.innerHeight}px 0rem 0rem 0rem`,
                opacity: 0,
            })
            break;
    }
}