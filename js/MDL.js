const selectorHTML = (html) => {
    if ((typeof html) == 'string') {
        return document.querySelector(html)
    } else {
        return html
    }
}

class html {
    constructor(element) {
        this.html = selectorHTML(element);
    }

    //STYLE
    css(styles = {}) {
        Object.keys(styles).map((styleKey) => this.html.style[styleKey] = styles[styleKey]);
    }

    attributeExeptions(exeption) {
        if (exeption == 'class') return 'className'
        return exeption
    }

    attribute(attribute = {}) {
        Object.keys(attribute).map((attributeKey) => this.html[this.attributeExeptions(attributeKey)] = attribute[attributeKey])
    }

    changeClass(className, newClassName) {
        this.html.className = this.html.className.replace(className, newClassName)
    }

    //INNER HTML TEXT
    inText(innerText = '') {
        this.html.innerHTML = innerText;
    }

    //PLAIN TEXT
    text(text = '') {
        this.html.textContent = text;
    }

    //APEND AN ELEMENT TO THIS
    append(child) {
        this.html.append(selectorHTML(child))
    }

    //APEND THIS TO ANOTHER HTML ELEMENT
    appendThis(father) {
        selectorHTML(father).append(this.html)
    }

    //APPEND AN ARR OF ELEMENTS
    multiAppend(arr_html) {
        arr_html.map(html => this.append(selectorHTML(html)))
    }

    //ADD CLICK EVENT
    click(cb) {
        this.html.addEventListener('click', () => cb())
    }

    //ADD CLICK EVENT AND SEND AS A PARAMETER TO THE CALLBACK THE EVENT
    clickPlus(cb) {
        this.html.addEventListener('click', (e) => cb(e))
    }

    //DISABLE HOVER 
    disableHover() {
        this.html.addEventListener('mouseenter', () => { })
        this.html.addEventListener('mouseleave', () => { })
    }

    //ADD AN HOVER EVENT
    hover(cbIn, cbOut) {
        this.html.addEventListener('mouseenter', () => cbIn())
        this.html.addEventListener('mouseleave', () => cbOut())
    }
    //ADD AN HOVER EVENT AND SEND AS A PARAMETER TO THE CALLBACK THE EVENT
    hoverPlus() {
        this.html.addEventListener('mouseenter', (e) => cbIn(e))
        this.html.addEventListener('mouseleave', (e) => cbOut(e))

    }

    //REMOVE THIS ELELEMT FROM THE DOM
    removeThis() {
        this.html.remove();
    }


    //ADD AN EVENT
    event(event = '', cb) {
        this.html.addEventListener(event, () => cb())
    }

    //ADD AN EVENT AND SEND AS A PARAMETER TO THE CALLBACK THE EVENT
    eventPlus(event = '', cb) {
        this.html.addEventListener(event, (e) => cb(e))
    }

    //ADD MULTIPLE EVENTS
    multievent() {

    }

    multieventPlus() {

    }

    //ADD AN ANIMATION TO THIS ELEMENT
    setAnimation() {

    }

}


const fastAppend = (child, father) => {
    father.append(child)
}

const createElement = (element) => {
    return document.createElement(element);
}

const doABR = (father = '') => {
    selectorHTML(father).append(createElement('br'))
}


const doXBR = (father = '', ammountOfBR) => {
    for (let i = 0; i <= ammountOfBR; i++) {
        selectorHTML(father).append(createElement('br'))
    }
}

const clean_div = (div) => {
    const nodesNumbers = div.childNodes.length;
    for (let i = 0; i < nodesNumbers; i++) {
        div.childNodes[0].remove()
    }
}

const fromHTMLCollecToArr = (className) => {
    return Array.prototype.slice.call(document.getElementsByClassName(className));

}

