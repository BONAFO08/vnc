const varDatabaseDir = path.join(__dirname, '/js/textCreator/var.database.json');

//bool
//int
//float
//string
//function
//none

const loadDatabase = async () => {
    try {
        const readFile = util.promisify(fs.readFile);
        const data = await readFile(varDatabaseDir);
        return { data: JSON.parse(data), bool: true, err: '' }
    } catch (error) {
        console.log(error);
        return { bool: false, err: error }
    }
}

const saveDatabase = async (database) => {
    try {
        const saveFile = util.promisify(fs.writeFile);
        const file_response = await saveFile(varDatabaseDir, JSON.stringify(database));
        return { bool: true, err: '' }
    } catch (error) {
        return { bool: false, err: error }
    }
}


const validateValue = (data) => {
    switch (data.type) {
        case 'bool':
            return (typeof data.value === 'boolean');

        case 'string':
            return (typeof data.value === 'string');

        case 'number':
            return (typeof data.value === 'number');

        case 'object':
            let validator = (typeof data.value == 'object')
            if (validator) {
                validator = Array.isArray(data.value);
                if (!validator) {
                    return true
                }
            }
            return false;

        case 'array':
            return Array.isArray(data.value);

        case 'none':
            return true;

        default:
            return false
    }
};


const dataValidator = (data) => {
    const validator = {
        bool: true,
        msj: 'SOME DATA ARE INVALID :'
    };

    if (data.id == undefined || data.id == '') {
        validator.bool = validator.bool && false;
        validator.msj = validator.msj + '\n-INVALID ID';
    } else {
        validator.id = data.id;
    }

    if (data.type == undefined || data.type == '') {
        validator.bool = validator.bool && false;
        validator.msj = validator.msj + '\n-INVALID VARTYPE';
    } else {
        validator.type = data.type;
    }

    //PERMITIR AL USER SELECCIONAR UN TIPO DE VAR Y DEJARLA VACIA 

    const valueValidated = validateValue(data);
    if (!valueValidated) {
        validator.bool = validator.bool && false;
        validator.msj = validator.msj + `\n-THE VALUE [${JSON.stringify(data.value)}] DOESN'T MATCH WITH THE VARTYPE SELECTED [${data.type}]`;
    }

    return validator
}



const createVariable = (data) => {
    const validator = dataValidator(data);
    if (validator.bool) {
        console.log('SAVED');
        // saveNewVar({
        //     id: data.id,
        //     type: data.type,
        //     value: data.value,
        // });
    } else {
        console.error(validator.msj);
    }

}


const defaultVariableValue = (data) => {
    switch (data.type) {
        case 'bool':
            return false;

        case 'string':
            return '';

        case 'number':
            return 0;

        case 'object':
            return {};

        case 'array':
            return [];

        case 'none':
            return undefined;

        default:
            console.error('INVALID varType')
            break;
    }
}

const asingNewType = async (newType, idVariable) => {
    let database = await loadDatabase();
    if (database.bool) {
        database = database.data;
        const searchVar = database.filter(variable => variable.id == idVariable)[0];
        if (searchVar != undefined) {
            const variableIndex = database.indexOf(searchVar);
            database[variableIndex].type = newType;
            if (validateValue(database[variableIndex])) {
                return await updateDatabase(database);
            } else {
                const newValue = defaultVariableValue(database[variableIndex]);
                const userConfirm = window.confirm(
                    `THE ACTUAL VALUE OF THE VARIABLE "${database[variableIndex].value}" IS INCOMPATIBLE WITH THE NEW varType,
                    ARE YOU SURE YOU WANT TO CHANGE IT? (THE OLD VALUE WILL BE CHANGED TO >>${newValue}<<)`
                );
                if (userConfirm) {
                    database[variableIndex].value = newValue;
                    return await updateDatabase(database);
                }
            }
        } else {
            console.error(`THE ID ${idVariable} IS INVALID`);
        }

    } else {
        console.error(database.err);
    }
}


const asingNewId = async (newID, idVariable) => {
    let database = await loadDatabase();
    if (database.bool) {
        database = database.data;
        const searchVar = database.filter(variable => variable.id == idVariable)[0];
        if (searchVar != undefined) {
            const validateID = database.filter(variable => variable.id == newID)[0];
            if (validateID == undefined) {
                const variableIndex = database.indexOf(searchVar);
                database[variableIndex].id = newID;
                return await updateDatabase(database);
            } else {
                console.error(`THE ID ${newID} IS ALREADY TAKEN`);
            }
        } else {
            console.error(`THE ID ${idVariable} IS INVALID`);
        }

    } else {
        console.error(database.err);
    }
}

const asignNewValue = async (newValue, idVariable) => {
    let database = await loadDatabase();
    if (database.bool) {
        database = database.data;
        const searchVar = database.filter(variable => variable.id == idVariable)[0];
        if (searchVar != undefined) {
            const valueValidator = validateValue({
                type: searchVar.type,
                value: newValue
            });
            if (valueValidator) {
                const variableIndex = database.indexOf(searchVar);
                database[variableIndex].value = newValue;
                return await updateDatabase(database);
            } else {
                console.error(`THE NEW VALUE ${newValue} IS A/AN ${typeof newValue} AND THIS VARIABLE ONLY ACCEPTS ${searchVar.type} VALUES`);
            }


        } else {
            console.error(`THE ID ${idVariable} IS INVALID`);
        }

    } else {
        console.error(database.err);
    }
}



const updateDatabase = async (newDatabase) => {
    return await saveDatabase(newDatabase);
}

const saveNewVar = async (newVariable) => {
    let database = await loadDatabase();
    if (database.bool) {
        database = database.data
        const theIDexist = database.filter(variable => variable.id == newVariable.id)[0];
        if (theIDexist == undefined) {
            database.push({
                id: newVariable.id,
                type: newVariable.type,
                value: newVariable.value,
            })
            const response = await saveDatabase(database);
        } else {
            console.error(`THE ID [${newVariable.id}] IS ALREADY TAKEN`);
        }
    } else {
        console.error(database.err);
    }

    // database.push({
    //     id: this.id,
    //     type: this.type,
    //     value: this.value,
    // })


}

const deleteVariable =(idVariable)=>{

}

//  cosa = () => {
// alert(21)
// };

// const vasito = new Variable();
// vasito.createVariable({
//     id: 'Naomi_Name',
//     type: 'function',
//     value: 'cosa'

// });


// console.log(window["cosa"]);



// var adder = new Function('alert(a)');s
// adder.arguments = ('a')


// let perro =`()=>{
//     alert(1)
// }
// `

// perro = JSON.parse(JSON.stringify(perro));
// console.log(perro() );