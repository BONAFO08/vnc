const clearInputs = () => {
    new html('#varType').html.value = '';
    new html('#varID').html.value = '';
    new html('#varValue').html.value = '';
    new html('#varComment').html.value = '';
}



//BOOL => APARECEN 2 BOTONES => FALSE // TRUE 
//AL TOCAR UNO QUEDA MARCADO
//EMPTY ==> DESAPARECE EL INPUT
const changeInput = (varType) => {
    const inputValue = new html('#varValue');


    switch (varType) {
        case 'number':
            inputValue.attribute({
                type: 'number'
            })
            break;

        case 'string':
        case 'object':
        case 'array':
            inputValue.attribute({
                type: 'text'
            })
            break;

    }
}




{/* <option class="selector-var-type-options" value="bool">Boolean</option>
<option class="selector-var-type-options" value="string">String</option>
<option class="selector-var-type-options" value="number">Number</option>
<option class="selector-var-type-options" value="object">Object</option>
<option class="selector-var-type-options" value="array">Array</option>
<option class="selector-var-type-options" value="none">Empty</option> */}

new html('#button-save').click(() => {

})

new html('#varType').eventPlus('change', (e) => {
    changeInput(e.target.value);
})


new html('#button-cancel').click(() => {
    const userConfirm = vex.dialog.open({
        unsafeMessage: `What do you want to do?`,
        buttons: [
            {
                className: 'btn btn-danger',
                text: 'CLEAR INPUTS',
                click: () => {
                    clearInputs();
                }

            },
            {
                className: 'btn btn-warning',
                text: 'GO HOME',
                click: () => {
                    window.location.href = './index.html';
                }

            },

            {
                className: 'btn btn-primary',
                text: 'CANCEL',
            },
        ],
        overlayClosesOnClick: false,
    })

    //  console.log( vex.dialog.confirm({
    //     message: 'Are you absolutely sure you want to destroy the alien planet?',
    //     callback: function (value) {
    //         if (value) {
    //             console.log('Successfully destroyed the planet.')
    //         } else {
    //             console.log('Chicken.')
    //         }
    //     }
    // }));

    userConfirm.contentEl.className = 'vex2'
    userConfirm.options.className = ' vex2';


})

